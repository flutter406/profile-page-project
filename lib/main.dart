import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      iconTheme: IconThemeData(color: Colors.indigo.shade500),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      iconTheme: IconThemeData(color: Colors.white),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppbarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.sunny),
          onPressed: () {
            setState(() {
              if(currentTheme == APP_THEME.DARK) {
                currentTheme = APP_THEME.LIGHT;
              }else {
              currentTheme = APP_THEME.DARK;
              }
            });
          },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          // color: Colors.black87,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.black87,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.black87,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.black87,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.black87,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.black87,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

//ListTitle
Widget mobilePhoneListTile() {
  return ListTile(
      leading: Icon(Icons.call),
      title: Text("0886449076"),
      subtitle: Text("mobile"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        // color: Colors.black87,
        onPressed: () {},
      ));
}

Widget otherPhoneListTile() {
  return ListTile(
      leading: Text(""),
      title: Text("-"),
      subtitle: Text("other"),
      trailing: IconButton(
        icon: Icon(Icons.message),
        // color: Colors.black87,
        onPressed: () {},
      ));
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160285@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: Text(""),
  );
}

Widget addressListTile() {
  return ListTile(
      leading: Icon(Icons.location_pin),
      title: Text("Nonthaburi, Thailand"),
      subtitle: Text("home"),
      trailing: IconButton(
        icon: Icon(Icons.directions),
        // color: Colors.black87,
        onPressed: () {},
      ));
}

AppBar buildAppbarWidget() {
  return AppBar(
    backgroundColor: Colors.black54,
    leading: Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(onPressed: () {}, icon: Icon(Icons.star_border_outlined)),
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 270,

            child: Image.network(
              "https://lh3.googleusercontent.com/fife/AAbDypC5khfRvb3IxOv-6IsaVFkiMsvHlfaiBAlPlBBgzx-SFn-oXgA6av7lnD_pE4NDxMf4w72gMTezyP2NvL7EwMP3uMg8tk6Lftb5unkoHT5WZ2kHAPe7jwE7raMLKKeen5nPrxR_-1gsVR1vjOR5fys1H50SkN5chxUrjbdg6JDVCR8xQ0PwuVFZP-Ioaklmtvyv8sDhmX1lWhvIje_--V_1LPOlubxTWsJKiiexGg5y50pht-0wBKqp7bkeRv24uGVnTb6XuLdvzJtijPqm5lM8qVMm8MczOYHundF1nLroLxWOEclBD8AMxGXdfeFhXBeKkMvAY_lfSJEVv_mbJLTVc8NuWImTY62ul5fii7ScPD7EtWZIse05s0UmO22BCBrqcHkJqbq1x0YYZWoQBp5y8UsGHOaXHd8GZLrQ-c9dVmULvmnYJXAcGqfOblHzp7oF4GBJtgabLbVIVA4_Faa5kDJN82yW7DZMpc8gpoJ3S2-Uc2RkAtltldle8uNlPCzrhXZ3SCjq-LaTNza-wtumT9Lg96suxxeqGowOQ9x6R-iIS5rKRJKKXbEob1zM3PuLaFTNoGT6M_Fw0CkCnhumdA7_mO1TqHEQj5hm4ikjmKnMSbQUcoqOsACmAp-z_Iq6ohoToc0-j2j0nO2jH2lcnsfSgljMOn1Bnmp7pQeYS070LvO_kINLGPiNgCGjRweQoYcCdxwUAFbFEKckr5nx0hlkSBkbLK6GwbtFi6EQ1ueMsGDSqy0pwMw6dDcsMaBAA2rdFAUSZfw-PwN4ba6kr1LaYm0WtFcssXPDfeohqw_P3IhVKORXkr9XdISkbCS3Jg7wKOPN1dCrS_a3QVumjwvgEcDZpmZtsLusL8CgPp-OFwYcWvs3PnT31obI4R-G_XI_zq9deqjs9qBU1IM9T9QjBl6apjPnfDIDR7KzsqKY5jA-7ffKDe4b1B0JEUrlqKz35ZiO7qvsb1g4Q7XwefX13f3TdXq2eFjecO8yMtOdE0b-6Ksyirw4etzkEsyLK-mQ20rPyhkVsWPN8X9oBCRHe4UORORnsRbc7G1w98GCfSvZgHcowq8T-vmDwUXdZAZcyVtEkAlhwnI=w1920-h854?authuser=1",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Benyapa Bunwisoot",
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
            thickness: 0.3,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.grey,
                  )),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.black,
            thickness: 0.7,
          ),
          mobilePhoneListTile(),
          // otherPhoneListTile(),
          Divider(
            color: Colors.black,
            thickness: 0.3,
          ),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}
